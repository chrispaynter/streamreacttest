# Setup

The Stream SDK has not been included. this is a fresh React Native project.

You'll need node to run the npx commands if you want to run the app.

More setup instructions [here if needed](https://facebook.github.io/react-native/docs/getting-started). Note be sure to use the "React Native CLI Quickstart".

Alternatively, if you just want to look at the xCode workspace it's in the iOS folder.

The main issue we have is that we can't build the application with the Stream SDK for real iPhones and the Generic iOS device. So you may not need to run the app at all, just get it building.

This is the error we get:

```
Undefined symbols for architecture arm64:
  "WV::ConfigDictionary::sEmptyDictionary", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::AllocateSubDictionary()", referenced from:
      WV::ConfigDictionary::ConfigDictionary() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::Load(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char> >)", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::~ConfigDictionary()", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::GetStringValue(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char> >, WV::ConfigDictionary const&) const", referenced from:
      WV::ConfigDictionary::operator()(std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char> >, WV::ConfigDictionary const&) const in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::GetSubDictionaryNames() const", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::GetKeys() const", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "WV::ConfigDictionary::operator[](std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char> >) const", referenced from:
      WVLogServer::ReadConfig() in libWViPhoneAPI.a(WVLogServer.o)
  "_res_9_init", referenced from:
      PilGetHostAddressByName(char const*) in libWViPhoneAPI.a(PilSocket.o)
ld: symbol(s) not found for architecture arm64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
```

# Install dependencies

From the root directory

```
yarn
cd ios
pod install
```

# To run

### iOS

```
npx react-native run-ios
```

### Android

```
npx react-native run-android
```
